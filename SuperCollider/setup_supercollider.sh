#!/bin/bash
git clone --recurse-submodules https://github.com/SuperCollider/SuperCollider.git
cd SuperCollider
mkdir build

# Configure compilation (release compilation, nonstandard install dir, use native optimizations for this machine, no emacs, no visual input, no QT, no other thingy)
cmake -DNO_X11=ON -DNO_AVAHI=ON -DCMAKE_BUILD_TYPE=Release -DNATIVE=ON -DCMAKE_INSTALL_PREFIX=../installation -DSC_EL=no -DSC_QT=OFF

# Compile with 9 cores (cores + 1)
make -j9

make install

# Add installation to path
# export PATH=$PATH:.../SC3_Portable/installation/bin

