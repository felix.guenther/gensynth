import operator
import random
from functools import partial

from deap import gp

from gensynth.operators.customized_operators import mutateLayerWeighted, mergeTrees, cxSimulatedBinary, rerollParameters


def apply_genetic_operators(population, config, primitive_set, toolbox):
    ea_config = config['evolutionary_algorithm']
    gp_config = ea_config['genetic_programming']
    operator_config = ea_config['operators']

    # Clone offspring so we can change it
    offspring = list(map(toolbox.clone, population))

    # Bloat control: Prevent genetic operators generating trees that are too large
    limit_decorator = gp.staticLimit(
        key=operator.attrgetter("height"),
        max_value=gp_config['tree_depth']['max']
    )

    # Set up dicts of operators. A subset of these (according to the config) will be applied.
    crossover_operators = {
        'one_point': limit_decorator(gp.cxOnePoint),
        'merge': limit_decorator(partial(
            mergeTrees, primitive_set=primitive_set, jax_config=config['jax'], only_arithmetic=False
        )),
        'merge_arithmetic': limit_decorator(partial(
            mergeTrees, primitive_set=primitive_set, jax_config=config['jax'], only_arithmetic=True
        ))
    }

    mutation_operators = {
        'uniform': limit_decorator(partial(
            gp.mutUniform, expr=toolbox.generateSynthTree, pset=primitive_set
        )),
        'shrink': gp.mutShrink,
        'replace': partial(
            gp.mutNodeReplacement, pset=primitive_set
        ),
        'insert': limit_decorator(partial(
            gp.mutInsert, pset=primitive_set
        )),
        'layer_weighted': limit_decorator(partial(
            mutateLayerWeighted, expr=toolbox.generateSynthTree, pset=primitive_set
        ))
    }

    # Different operators are available for parameters, depending on the implementation.
    if config['jax']['use_jax_implementation']:
        crossover_operators['parameter_simulated_binary'] = partial(
            cxSimulatedBinary,
            eta=config['jax']['simulated_binary_crossover_eta']
        )
        mutation_operators['parameter_reroll'] = partial(
            rerollParameters, config=config
        )
    else:
        mutation_operators['ephemeral'] = partial(
            gp.mutEphemeral, mode=config['supercollider']['eph_mode']
        )

    # Apply crossover, randomly selecting an operator for each case
    if len(operator_config['crossover']) > 0:
        for i in range(1, len(offspring), 2):
            if random.random() < operator_config['crossover_probability']:
                crossover_operation = crossover_operators[random.choice(operator_config['crossover'])]

                result = crossover_operation(offspring[i - 1], offspring[i])
                if len(result) == 2:
                    child_1, child_2 = result
                else:
                    child_1 = result[0]
                    child_2 = None

                offspring[i] = child_1
                del offspring[i].fitness.values

                # The second child may be None if the merge operator was applied, which only generates one child.
                if child_2 is not None:
                    offspring[i - 1] = child_2
                    del offspring[i - 1].fitness.values

    # Apply mutation, randomly selecting an operator for each case
    if len(operator_config['mutation']) > 0:
        for i in range(len(offspring)):
            if random.random() < operator_config['mutation_probability']:
                mutation_operation = mutation_operators[random.choice(operator_config['mutation'])]

                offspring[i], = mutation_operation(offspring[i])
                del offspring[i].fitness.values

    return offspring
