from deap import tools

from gensynth.operators.customized_selection import selDoubleTournament, selNonDominatedRankDoubleTournament


def perform_selection(offspring, config, hall_of_fame):
    use_multiobjectivization = (len(config['evolutionary_algorithm']['fitness_function']['feature']) > 1)

    # Perform selection from both parents and offspring
    if not use_multiobjectivization:
        offspring = perform_single_objective_selection(offspring, hall_of_fame, config)
    else:
        offspring = perform_multi_objective_selection(offspring, hall_of_fame, config)
    return offspring


def perform_single_objective_selection(offspring, hall_of_fame, config):
    ea_config = config['evolutionary_algorithm']
    selection_config = ea_config['selection']

    if selection_config['scheme'] == 'tournament':
        offspring = tools.selTournament(
            offspring,
            # Leave space for elitism
            k=ea_config['population_size'] - selection_config['hall_of_fame_size'],
            tournsize=selection_config['fitness_tournament_size']
        )
    elif selection_config['scheme'] == 'double_tournament':
        offspring = tools.selDoubleTournament(
            offspring,
            # Leave space for elitism
            k=ea_config['population_size'] - selection_config['hall_of_fame_size'],
            fitness_size=selection_config['fitness_tournament_size'],
            parsimony_size=selection_config['size_tournament_size'],
            fitness_first=True
        )
    elif selection_config['scheme'] == 'rank_double_tournament':
        offspring = selDoubleTournament(
            offspring,
            # Leave space for elitism
            k=ea_config['population_size'] - selection_config['hall_of_fame_size'],
            fitness_size=selection_config['fitness_tournament_size'],
            parsimony_size=selection_config['size_tournament_size'],
            fitness_first=True
        )

    if hall_of_fame is not None:
        # Apply elitism
        offspring.extend(hall_of_fame.items)

    return offspring


def perform_multi_objective_selection(offspring, hall_of_fame, config):
    ea_config = config['evolutionary_algorithm']
    selection_config = ea_config['selection']

    if ea_config['fitness_function']['nd_rank_double_tournament_selection'] in [None, False]:
        # NSGA2 automatically applies elitism, as we use the combined population and the best are always selected.
        offspring = tools.selNSGA2(
            offspring,
            k=ea_config['population_size']
        )
    else:
        offspring = selNonDominatedRankDoubleTournament(
            offspring,
            # Leave space for elitism
            k=ea_config['population_size'] - selection_config['hall_of_fame_size'],
            fitness_size=selection_config['fitness_tournament_size'],
            parsimony_size=selection_config['size_tournament_size'],
            fitness_first=True
        )

        # Apply elitism
        offspring.extend(hall_of_fame.items)
    return offspring