from functools import partial
from itertools import chain
from operator import attrgetter
from random import random

from deap.tools import selRandom, sortNondominated
from deap.tools.emo import assignCrowdingDist


def _sizeTournament(individuals, k, select, parsimony_size):
    chosen = []
    for i in range(k):
        # Select two individuals from the population
        # The first individual has to be the shortest
        prob = parsimony_size / 2.
        ind1, ind2 = select(individuals, k=2)

        if len(ind1) > len(ind2):
            ind1, ind2 = ind2, ind1
        elif len(ind1) == len(ind2):
            # random selection in case of a tie
            prob = 0.5

        # Since size1 <= size2 then ind1 is selected
        # with a probability prob
        chosen.append(ind1 if random() < prob else ind2)

    return chosen


# Custom double tournament that performs rank selection on a random subset for the fitness tournament.
# This is intended to decrease the selection pressure.
def selDoubleTournament(individuals, k, fitness_size, parsimony_size, fitness_first, fit_attr="fitness"):
    assert (1 <= parsimony_size <= 2), "Parsimony tournament size has to be in the range [1, 2]."

    def _fitTournament(individuals, k, select):
        chosen = []
        for _ in range(k):
            aspirants = select(individuals, k=fitness_size)

            # Sort from high fitness to low fitness
            sorted_aspirants = sorted(aspirants, key=attrgetter(fit_attr))
            last_rank = len(sorted_aspirants)
            # Gaussian sum formula
            total_rank_sum = (last_rank**2 + last_rank)/2

            u = random() * total_rank_sum
            sum_ = 0
            for index, individual in enumerate(sorted_aspirants):
                rank = index + 1
                sum_ += rank
                if sum_ > u:
                    chosen.append(individual)
                    break

        return chosen

    if fitness_first:
        tfit = partial(_fitTournament, select=selRandom)
        return _sizeTournament(individuals, k, tfit, parsimony_size)
    else:
        tsize = partial(_sizeTournament, select=selRandom)
        return _fitTournament(individuals, k, tsize)


# Custom NSGA-2 variant that doesn't select the top solutions, but performs double tournament
# Selection once it has the sorted list.
# The idea is that this keeps some sort of bloat control active.
def selNonDominatedRankDoubleTournament(individuals, k, fitness_size, parsimony_size, fitness_first, fit_attr="fitness"):
    assert (1 <= parsimony_size <= 2), "Parsimony tournament size has to be in the range [1, 2]."

    pareto_fronts = sortNondominated(individuals, k)
    for front in pareto_fronts:
        assignCrowdingDist(front)
        # Sort within the front in ascending order
        # This means that more crowded solutions (smaller crowding distance)
        # appear at the top, and less crowded ones at the bottom.
        front.sort(key=attrgetter("fitness.crowding_dist"))

    # Reverse the front order to assign the highest index (which we use as fitness replacement) to the best front
    pareto_fronts.reverse()

    flattened_individuals = list(chain(*pareto_fronts))

    def _fitTournament(individuals, k, select):
        chosen = []
        for _ in range(k):
            number_individuals = len(individuals)
            individual_indices = list(range(number_individuals))
            aspirant_indices = select(individual_indices, k=fitness_size)

            # Each index needs to be increased by one to get the rank, so that index zero still has
            # a chance of being selected.
            total_rank_sum = sum(aspirant_indices) + fitness_size

            u = random() * total_rank_sum
            sum_ = 0
            for aspirant_index in aspirant_indices:
                rank = aspirant_index + 1
                sum_ += rank
                if sum_ > u:
                    individual = individuals[aspirant_index]
                    chosen.append(individual)
                    break

        return chosen

    if fitness_first:
        tfit = partial(_fitTournament, select=selRandom)
        return _sizeTournament(flattened_individuals, k, tfit, parsimony_size)
    else:
        tsize = partial(_sizeTournament, select=selRandom)
        return _fitTournament(flattened_individuals, k, tsize)
