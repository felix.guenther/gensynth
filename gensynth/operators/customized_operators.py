from random import choice

import numpy as np
from deap import creator

from gensynth.differentiable.initialization import generate_random_parameters


def mutateLayerWeighted(individual, expr, pset):
    node_count = len(individual)
    weights = np.empty(shape=(node_count,))

    stack = [0]
    for index, node in enumerate(individual):
        depth = stack.pop()
        # TODO: Try out other (eg. linear or faster) weights. This would be correct for a complete
        #  binary tree, which the syntax tree isn't.
        weights[index] = 1/(3**depth)
        # Add depths for the children
        stack.extend([depth + 1] * node.arity)

    # Normalize weights
    weights /= np.sum(weights)

    index = np.random.choice(node_count, p=weights)

    slice_ = individual.searchSubtree(index)
    type_ = individual[index].ret
    individual[slice_] = expr(pset=pset, type_=type_)
    return individual,


def rerollParameters(individual, config):
    individual.parameters = generate_random_parameters(config)
    return individual,


# A crossover-like operator that simply combines two trees with a binary primitive
# at the root. As a result, this only generates a single new solution, not two.
def mergeTrees(parent_1, parent_2, primitive_set, jax_config, only_arithmetic=False):
    # Find all binary primitives
    binary_primitives = []
    for primitive in primitive_set.primitives[float]:
        if primitive.arity == 2 and (not only_arithmetic or primitive.name in ['add', 'mul', 'sub']):
            binary_primitives.append(primitive)

    # Select one primitive at random
    root_primitive = choice(binary_primitives)

    # Build the new expression with the root primitive at the top and the old
    # individuals as the child nodes
    expression = [root_primitive]
    expression.extend(parent_1)
    expression.extend(parent_2)

    child = creator.Individual(expression)

    if jax_config['use_jax_implementation']:
        # Since we have to provide new parameters for the child, perform crossover and
        # assign one of the resulting parameter sets to the child.
        parameters, _ = cxSimulatedBinary(
            parent_1.parameters,
            parent_2.parameters,
            jax_config['simulated_binary_crossover_eta']
        )
        child.parameters = parameters

    # The interface for the other crossover methods expects we return two a tuple
    return (child, )


# Implementation of simulated binary crossover (extended from the DEAP one)
# that copies the values instead of modifying them in place.
def cxSimulatedBinary(ind1, ind2, eta):

    rand = np.random.rand(len(ind1))
    beta = np.empty(len(ind1))
    beta[rand <= 0.5] = 2. * rand[rand <= 0.5]
    beta[rand > 0.5] = 1. / (2. * (1. - rand[rand > 0.5]))

    beta **= 1. / (eta + 1.)
    new_ind1 = 0.5 * (((1 + beta) * ind1) + ((1 - beta) * ind2))
    new_ind2 = 0.5 * (((1 - beta) * ind1) + ((1 + beta) * ind2))

    return new_ind1, new_ind2
