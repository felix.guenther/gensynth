from typing import List

import numpy as np
from ray.air import session


class TuneLogging:

    def log_generation(self, generation: int, population: List):

        fitnesses = np.array([individual.fitness.values[0] for individual in population])
        best_fitness = np.min(fitnesses)
        best_individual = population[np.argmin(fitnesses)]
        session.report({
            "generation": generation,
            "best_fitness": best_fitness,
            # Only save the string to minimize storage usage.
            "best_individual": str(best_individual)
        })

    def log_lifetime_learning(self, learning_loss_differences, jit_loss_differences):
        return
