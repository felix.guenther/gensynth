import os
import pickle
import shutil
from typing import List


class PickleLogging:

    def __init__(self, run_name, config_path, logging_dir='./logs/pickle/'):
        self.logging_dir = logging_dir + run_name

        if not os.path.exists(self.logging_dir):
            os.makedirs(self.logging_dir)

        shutil.copy(
            config_path,
            self.logging_dir + '/config.yaml'
        )

    def log_generation(self, generation: int, population: List):
        # Delete data derived from the syntax tree to save storage space
        for individual in population:
            individual.audio = None
            individual.synth_definition = None

        output_file = open(f'{self.logging_dir}/{generation:02d}.pkl', 'wb')
        pickle.dump(population, output_file)
        output_file.close()

    def log_lifetime_learning(self, learning_loss_differences, jit_loss_differences):
        return
