import io
from typing import List, Dict

import librosa
from deap import gp
from librosa.feature import melspectrogram

import numpy as np
import tensorflow as tf
import pygraphviz as pgv
import matplotlib.pyplot as plt


class TBLogging:

    def __init__(self, run_name, sampling_rate, use_jax_implementation, use_multiobjectivization, logging_dir='./logs/tensorboard/'):
        self.sampling_rate = sampling_rate
        self.use_jax_implementation = use_jax_implementation
        self.use_multiobjectivization = use_multiobjectivization
        file_writer = tf.summary.create_file_writer(logging_dir + run_name)
        file_writer.set_as_default()
        tf.summary.experimental.set_step(0)

    def log_generation(self, generation: int, population: List):

        fitnesses = np.array([individual.fitness.values[0] for individual in population])

        for stat in [np.min, np.median]:
            tf.summary.scalar('individual fitness ' + stat.__name__, stat(fitnesses),)

        depths = [individual.height for individual in population]
        for stat in [np.max, np.min, np.mean, np.std]:
            tf.summary.scalar('depth ' + stat.__name__, stat(depths))

        invalid_individual_count = len(list(filter(lambda fitness: fitness == np.inf, fitnesses)))
        tf.summary.scalar('invalid individual count', invalid_individual_count)

        tf.summary.experimental.set_step(generation + 1)

        n_best = 3
        k_samples = 5

        # Compute the indexes of the n smallest fitnesses
        best_indexes = np.argpartition(fitnesses, n_best)[:n_best]
        best_indexes = np.sort(best_indexes)

        audios = [individual.audio for individual in population]

        # Add dimension for multiple channels that TensorBoard requires
        expanded_audios = np.expand_dims(audios, 2)

        best_audios = expanded_audios[best_indexes]

        tf.summary.audio('best audios', best_audios, sample_rate=self.sampling_rate, max_outputs=n_best)
        tf.summary.audio('sample audios', expanded_audios, sample_rate=self.sampling_rate, max_outputs=k_samples)

        for i in range(n_best):
            index = best_indexes[i]
            individual = population[index]

            # Log the spectrograms for the best solutions
            mel_spectrogram = melspectrogram(y=np.array(individual.audio), sr=self.sampling_rate)
            figure = plot_mel_spectrogram(mel_spectrogram, self.sampling_rate)
            spectrogram_image_tensor = figure_to_image_tensor(figure)
            tf.summary.image('best spectrogram ' + str(i), spectrogram_image_tensor)

            # Log the graphs for the best solutions
            if not self.use_jax_implementation:
                graph_image_tensor = supriya_synth_definition_to_graph_image_tensor(individual.synth_definition)
            else:
                graph_image_tensor = individual_to_graph_image_tensor(individual)

            tf.summary.image('best graph ' + str(i), graph_image_tensor)

        if self.use_multiobjectivization:
            figure = plot_pareto_front(population)
            pareto_front_image_tensor = figure_to_image_tensor(figure)
            tf.summary.image('pareto front', pareto_front_image_tensor)

    def log_lifetime_learning(self, learning_loss_differences, jit_loss_differences):
        tf.summary.histogram('lifetime learning loss difference', np.array(learning_loss_differences))
        tf.summary.histogram('jitting loss difference', np.array(jit_loss_differences))
        tf.summary.scalar('average lifetime learning loss difference', np.mean(learning_loss_differences))


def individual_to_graph_image_tensor(individual):
    nodes, edges, labels = gp.graph(individual)

    graph = pgv.AGraph()
    graph.add_nodes_from(nodes)
    graph.add_edges_from(edges)
    graph.layout(prog="dot")

    for i in nodes:
        node = graph.get_node(i)
        node.attr["label"] = labels[i]

    png_image = graph.draw(format='png', prog="dot")

    # Convert the image to a Tensorboard-friendly format
    image_tensor = tf.image.decode_png(png_image)
    image_tensor = tf.expand_dims(image_tensor, 0)
    return image_tensor


def supriya_synth_definition_to_graph_image_tensor(synth_definition):
    # Generate a graph using graphviz
    uqbar_graph = synth_definition.__graph__()
    graph_string = format(uqbar_graph, "graphviz")
    graph = pgv.AGraph(graph_string)
    png_image = graph.draw(format='png', prog="dot")

    # Convert the image to a Tensorboard-friendly format
    image_tensor = tf.image.decode_png(png_image)
    image_tensor = tf.expand_dims(image_tensor, 0)
    return image_tensor


def plot_mel_spectrogram(mel_spectrogram, sampling_rate):
    mel_spectrogram_decibel = librosa.power_to_db(mel_spectrogram, ref=np.max)

    # Set up the plot
    figure, ax = plt.subplots()
    img = librosa.display.specshow(
        mel_spectrogram_decibel,
        x_axis='time',
        y_axis='mel',
        sr=sampling_rate,
        fmax=8000,
        ax=ax
    )
    figure.colorbar(img, ax=ax, format='%+2.0f dB')
    plt.tight_layout()
    ax.set(title='Mel-frequency spectrogram')

    return figure


def plot_pareto_front(population):
    objectives = np.array([individual.fitness.values for individual in population])

    figure, ax = plt.subplots()

    # Add the origin/utopian point
    ax.scatter(0, 0, c="k", marker="+", s=100)

    # Add the individual results
    ax.scatter(objectives[:, 0], objectives[:, 1], marker="o", s=48)

    ax.set_xlabel("Fitness 1")
    ax.set_ylabel("Fitness 2")
    ax.set_title('Pareto Front')

    ax.autoscale(tight=True)
    plt.tight_layout()

    return figure


def figure_to_image_tensor(figure):
    # Save the plot to a PNG in memory.
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    # Closing the figure prevents it from being displayed directly
    plt.close(figure)
    buf.seek(0)

    # Convert PNG buffer to TF image
    image = tf.image.decode_png(buf.getvalue(), channels=4)

    # Add the batch dimension
    image = tf.expand_dims(image, 0)
    return image
