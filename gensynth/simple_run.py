import argparse

from gensynth.genetic_algorithm import run_genetic_algorithm

# Simple script for running a single config specified via command line argument

parser = argparse.ArgumentParser()
parser.add_argument("--config", type=str, required=True)
args = parser.parse_args()
config_path = args.config

run_genetic_algorithm(config_path)
