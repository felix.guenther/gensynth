import jax
import jax.numpy as jnp
import optax
from jax import jit
from tqdm import tqdm

from gensynth.differentiable.fitness import compute_sound_losses
from gensynth.differentiable.synthesis import individual_to_sound
from gensynth.operators.selection import perform_selection


def apply_lifetime_learning(offspring, label_audio, duration, primitive_set, config, toolbox, logging):
    memetic_config = config['jax']['lifetime_learning']
    ea_config = config['evolutionary_algorithm']
    # Convert to tuple to make it hashable
    fitness_features = tuple(ea_config['fitness_function']['feature'])
    fitness_distance = ea_config['fitness_function']['distance']
    sampling_rate = config['target_audio']['sample_rate']

    if len(fitness_features) > 1 or 'mel_db' not in fitness_features:
        raise NotImplementedError("Lifetime Learning only works for mel log right now!")

    print("- Lifetime Learning")

    # Select the individuals to be used for lifetime learning using the same selection strategy as the main loop,
    # but without elitism as we don't have the hall of fame (and don't want to overtrain those solutions).
    learners = perform_selection(offspring, config, hall_of_fame=None)
    # Only keep the first couple, as the selection mechanism spits out an entire population
    learners = learners[:memetic_config['individual_count']]

    # Remove the individuals we want to improve from offspring and clone them so we don't change the originals
    offspring = [ind for ind in offspring if ind not in learners]
    learners = list(map(toolbox.clone, learners))

    # Initialize optimizer providing step sizes
    optimizer = optax.adam(learning_rate=float(memetic_config['initial_learning_rate']))

    for individual in tqdm(learners, desc="Performing Gradient Descent"):
        # Can't apply lifetime learning to invalid individuals
        if jnp.isinf(individual.fitness.values[0]):
            continue

        def parameter_dependent_loss(x):
            output_audio = individual_to_sound(individual, x, primitive_set, duration, sampling_rate)
            loss = compute_sound_losses(label_audio, output_audio, fitness_features, fitness_distance, sampling_rate)
            # Always differentiate with respect to the first loss
            return loss[0]

        jit_loss = jit(parameter_dependent_loss)

        parameters = individual.parameters
        init_fitness = jit_loss(parameters)

        # Jitting changes the value of the fitness due to numeric optimizations:
        # https://jax.readthedocs.io/en/latest/faq.html#jit-changes-the-exact-numerics-of-outputs
        # Always jitting the individual functions fixes this, but makes the fitness evaluations much
        # slower, as most individuals are only executed once.
        # Even if this is something we can't fix, we can at least monitor it.
        # jit_loss_differences.append(individual.fitness.values[0] - init_fitness)

        derivative = jax.grad(jit_loss)
        optimizer_state = optimizer.init(parameters)
        for step in range(memetic_config['gradient_descent_steps']):
            # We could jit the stepping, but this didn't provide a significant performance benefit for the current setup.
            # The gradient computation is the expensive part, and already jitted.
            gradient = derivative(parameters)
            updates, optimizer_state = optimizer.update(gradient, optimizer_state)
            parameters = optax.apply_updates(parameters, updates)

        final_fitness = jit_loss(parameters)

        # FIXME It would probably be more fair to compare the initial non-jitted fitness to
        #   the final non-jitted fitness.
        learning_loss_difference = final_fitness - init_fitness

        # Only keep the changes if they improved the individual.
        if learning_loss_difference < 0 and not jnp.isnan(final_fitness):
            individual.parameters = parameters.block_until_ready()
            individual.fitness.values = (final_fitness.block_until_ready(),)

        del parameter_dependent_loss, jit_loss, derivative, gradient, updates, optimizer_state

    offspring.extend(learners)
    print("-- Lifetime Learning Done.")

    
    return offspring
