import numpy as np


def initialize_parameters(individuals, config):
    # Initialize parameters randomly
    for individual in individuals:
        individual.parameters = generate_random_parameters(config)


def generate_random_parameters(config):
    # Use a range from 0 to 20k for 50% of the parameters and 0 to 5 for the other 50%.
    # This makes it more likely to generate small values, which are useful when the parameter
    # is not used as a frequency. It's also closer to SC's generation of ephemeral constants.
    ranges = np.random.rand(config['jax']['parameter_count'])
    ranges[ranges > 0.5] = 20_000.0
    ranges[ranges < 0.5] = 5.0
    # Explicitly use float32, because otherwise NumPy provides double values, which we don't need
    return (ranges * np.random.rand(config['jax']['parameter_count'])).astype(np.float32)