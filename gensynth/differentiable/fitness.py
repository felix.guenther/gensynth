from functools import partial

import jax
import jax.numpy as jnp
import jax.scipy.signal as jsg
from librosa import filters
from tqdm import tqdm

from gensynth.differentiable.synthesis import individual_to_sound


def mel_spectrogram(audio, sample_rate):
    nfft = 2048
    # NOTE This has significant differences to the librosa implementation.
    #   Looks very similar visually, though.
    frequencies, segment_times, stft = jsg.stft(
        x=audio,
        # Attempt to match the default parameters for librosa
        nfft=nfft,
        # win length None=nfft
        nperseg=nfft,
        # hop length 512
        noverlap=2048 - 512,
        # window hann
        window='hann',
        # center true
        # pad mode constant
        boundary='constant',
        # librosa doesn't pad, so we don't either (otherwise, we get an extra column at the end)
        padded=False,
        # Unknown JAX params: Detrend, return_opnesided
    )

    # NOTE jnp.abs has a slight difference to np.abs, even when using an equivalent stft
    magnitudes = jnp.abs(stft)
    # librosa uses a power of 2 per default to get the power (leaving this as 1 would return the energy)
    squared_magnitudes = magnitudes**2.0

    # Acquire the mel basis/filter. This is constant, so we can use the librosa implementation.
    mel_basis = filters.mel(sr=sample_rate, n_fft=nfft)

    # Linear Algebra operations expressed using a transformation of dimensions
    mel_spectrogram = jnp.einsum("...ft,mf->...mt", squared_magnitudes, mel_basis, optimize=True)
    return mel_spectrogram


# Implementation adapted from librosas
def power_to_db(spectrogram):
    reference_value = jnp.max(spectrogram)

    minimum_threshold = 1e-10
    log_spectrogram = 10.0 * jnp.log10(jnp.maximum(minimum_threshold, spectrogram))
    log_spectrogram -= 10.0 * jnp.log10(jnp.maximum(minimum_threshold, reference_value))

    top_db = 80.0
    log_spectrogram = jnp.maximum(log_spectrogram, log_spectrogram.max() - top_db)

    return log_spectrogram


@partial(jax.jit, static_argnames=['norm'])
def compute_spectrogram_distance(output_batch_spectrograms, label_spectrogram, norm):
    # The (single) label is broadcast over the spectrograms for each output
    difference = label_spectrogram - output_batch_spectrograms

    if norm in ['l1', 'l1_l2']:
        distance_l1 = jnp.linalg.norm(
            difference,
            # The last axis is omitted to compute the norm for each time step separately
            axis=(0,),
            ord=1
        )

    if norm in ['l2', 'l1_l2']:
        distance_l2 = jnp.linalg.norm(
            difference.astype(float),
            # The last axis is omitted to compute the norm for each time step separately
            axis=(0,),
            ord=2
        )

    # Sum over all time steps
    if norm == 'l1_l2':
        # Use both an L1 norm and an L2 norm to penalize few larger differences more strongly than smaller ones (L2)
        # but also penalize values in [0, 1] (L1).
        total = jnp.sum(distance_l1 + distance_l2)
    elif norm == 'l1':
        total = jnp.sum(distance_l1)
    elif norm == 'l2':
        total = jnp.sum(distance_l2)
    else:
        raise ValueError(f"Unknown distance {norm}")

    return total


# Perform JIT compilation, where only the output audio varies (as all other parameters are in effect fixed at runtime)
@partial(jax.jit, static_argnames=['fitness_features', 'fitness_distance', 'sampling_rate'])
def compute_sound_losses(label_audio, output_audio, fitness_features, fitness_distance, sampling_rate):
    label_mel_spectrogram = mel_spectrogram(label_audio, sampling_rate)
    output_mel_spectrograms = mel_spectrogram(output_audio, sampling_rate)

    fitness_count = len(fitness_features)
    losses = jnp.empty(shape=(fitness_count,))

    loss_index = 0

    # Compute the array of losses
    if 'mel' in fitness_features:
        loss = compute_spectrogram_distance(output_mel_spectrograms, label_mel_spectrogram, fitness_distance)
        losses = losses.at[loss_index].set(loss)
        loss_index += 1

    if 'mel_db' in fitness_features:
        db_output_mel_spectrograms = power_to_db(output_mel_spectrograms)
        db_label_mel_spectrogram = power_to_db(label_mel_spectrogram)
        loss = compute_spectrogram_distance(db_output_mel_spectrograms, db_label_mel_spectrogram, fitness_distance)
        losses = losses.at[loss_index].set(loss)
        loss_index += 1

    if 'mfcc' in fitness_features:
        raise NotImplementedError("MFCC isn't implemented in JAX yet!")

    if 'zero_crossings' in fitness_features:
        raise NotImplementedError("Zero Crossings isn't implemented in JAX yet!")

    return losses


def reevaluate_individuals(
        individuals,
        label_audio,
        primitive_set,
        duration,
        sampling_rate,
        fitness_config,
        # Just here to match the interface to the SC variant
        temporary_path=None
):
    if len(individuals) == 0:
        return []

    for individual in tqdm(individuals, desc="Reevaluating Fitness"):
        output_audio = individual_to_sound(individual, individual.parameters, primitive_set, duration, sampling_rate)

        # Compute loss only for valid individuals
        if output_audio is not None:
            # TODO Vectorize this, as the loss is the same for all audios
            #  Currently, loss computation is fast enough anyways tho.
            losses = compute_sound_losses(
                label_audio,
                output_audio,
                # Convert to a tuple because arguments must be hashable, and lists aren't.
                tuple(fitness_config['feature']),
                fitness_config['distance'],
                sampling_rate
            )


            # Save the audio for logging purposes
            individual.audio = output_audio

        else:
            losses = jnp.repeat(jnp.inf, len(fitness_config['feature']))

            # Save dummy audio
            individual.audio = jnp.zeros(int(duration * sampling_rate))

        # DEAP requires the fitness to be a tuple.
        # See https://deap.readthedocs.io/en/master/tutorials/basic/part2.html
        individual.fitness.values = losses
