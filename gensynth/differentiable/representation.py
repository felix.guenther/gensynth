import operator

import deap.gp
from deap.gp import PrimitiveSetTyped, PrimitiveTree, genGrow

from gensynth.differentiable.synthesis import sine_wave

available_ugens = [
    (sine_wave, [float, float], float),
    #(pulse, [float, float], float),
    #(white_noise, [], float),
    #(brown_noise, [], float),
    #(band_pass_filter, [float, float, float], float),
    #(band_reject_filter, [float, float, float], float)
]


def get_primitive_set(parameter_count):
    primitive_set = PrimitiveSetTyped(
        "main",
        in_types=[float for _ in range(parameter_count)],
        prefix="x",
        ret_type=float
    )

    primitive_set.addPrimitive(operator.mul, [float, float], float)
    primitive_set.addPrimitive(operator.add, [float, float], float)
    primitive_set.addPrimitive(operator.sub, [float, float], float)

    for (ugen, parameter_types, return_type) in available_ugens:
        primitive_set.addPrimitive(ugen, parameter_types, return_type)

    return primitive_set


def test():
    primitive_set = get_primitive_set(5)
    # Generate a tree randomly
    generated_tree = PrimitiveTree(genGrow(primitive_set, min_=1, max_=5))

    code = str(generated_tree)
    print(code)

    function = deap.gp.compile(generated_tree, primitive_set)


