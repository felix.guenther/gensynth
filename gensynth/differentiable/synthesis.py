import jax.numpy as jnp
from deap import gp


def individual_to_sound(individual, parameters, primitive_set, duration, sampling_rate):
    # Define arguments required by the synthesis functions that aren't part of the syntax tree.
    # During jitting, the current value of these is integrated into the compiled function.
    # FIXME Using global variables like this isn't pretty, but I'm unsure how else to inject them
    global sample_count
    sample_count = int(duration * sampling_rate)
    global sample_rate
    sample_rate = sampling_rate

    synth_definition = gp.compile(individual, primitive_set)
    output_audio = synth_definition(*parameters)

    if len(output_audio.shape) == 0:
        return None

    return output_audio


# FIXME: This implementation does still not 100% match SCs values, but it gets close enough for the moment.
#   One thing that doesn't match is that SC uses int32 for the phases, but directly applying that here didn't
#   help with the results.

# SC Implementation uses "a wavetable lookup oscillator with linear interpolation"
# https://github.com/supercollider/supercollider/blob/ef627ce2c564fe323125234e4374c9c4b0fc7f1d/server/plugins/OscUGens.cpp
# Table size: 8192 samples
# Phase should be in +-8pi
def sine_wave(frequency, phase):
    # SuperCollider does not perform this but instead only supports values in a range +-8pi.
    # However, we can't exactly replicate that and this gets closest to the results from their implementation for valid
    # phases. (We also can't ensure the phase is in this range in general, as the syntax tree may produce all
    # sorts of weird things)
    phase = phase % (2 * jnp.pi)

    # Multiply with the ones array to ensure the correct dimension is used, in case the input is a scalar.
    frequency_step = frequency * jnp.ones(sample_count, dtype=jnp.single) * (2 * jnp.pi) / sample_rate

    # Compute the phase (without offset) for each step, essentially numerically integrating
    # Over the frequency to get the phases.
    # We subtract the first frequency because SC starts with a phase of zero.
    cumulative_phases = jnp.cumsum(frequency_step, dtype=jnp.single) - frequency_step[0]

    # Add the phase offset for each sample
    total_phase_offsets = cumulative_phases + phase

    # SuperCollider computes this using a lookup table
    result = jnp.sin(total_phase_offsets)

    return result


# Width specifies the pulse width ratio
# Width in [0, 1]
def pulse(frequency, width):
    return None


# Second order filter
# Frequency: The center frequency
# Reciprocal Q: where 1/Q = 1/(frequency/bandwidth) = bandwidth/frequency
def band_pass_filter(input, frequency, reciprocal_q):
    return None


# Second order filter
# Frequency: The cutoff frequency
# Reciprocal Q: bandwidth/frequency
def band_reject_filter(input, frequency, reciprocal_q):
    return None


def white_noise():
    return None


def brown_noise():
    return None


# Note: DDSP uses an "angular cumsum" to avoid phase errors audibly accumulating:
# https://github.com/magenta/ddsp/blob/761d61a5f13373c170f3a04d54bb28a1e2f06bab/ddsp/core.py#L800
# This is an adjusted variant of their implementation.
def angular_cumsum(angular_frequency, chunk_size=1000):
    sample_count = angular_frequency.shape[0]

    # Pad if needed.
    remainder = sample_count % chunk_size
    if remainder:
        pad_amount = chunk_size - remainder
        angular_frequency = jnp.pad(angular_frequency, [0, pad_amount])

    # Split input into chunks.
    length = angular_frequency.shape[0]
    n_chunks = int(length / chunk_size)
    chunks = jnp.reshape(angular_frequency, [n_chunks, chunk_size])
    phase = jnp.cumsum(chunks, axis=1)

    # Add offsets.
    # Offset of the next row is the last entry of the previous row.
    offsets = phase[:, -1:] % (2.0 * jnp.pi)
    # Add a zero at the start and remove the last element, shifting the offsets by one
    # (as we need to add them to the next chunk)
    offsets = jnp.append(jnp.array([[0]]), offsets, axis=0)
    offsets = offsets[:-1]

    # Offset is cumulative among the rows.
    offsets = jnp.cumsum(offsets, axis=0) % (2.0 * jnp.pi)
    phase = phase + offsets

    # Put back in original shape.
    phase = phase % (2.0 * jnp.pi)
    phase = jnp.reshape(phase, [length])

    # Remove padding if added it.
    if remainder:
        phase = phase[:sample_count]
    return phase
