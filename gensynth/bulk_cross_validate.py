import os

import ray
import yaml

from gensynth.genetic_algorithm import run_genetic_algorithm

# Disable killing due to OOM
os.environ['RAY_memory_monitor_refresh_ms'] = '0'

ray.init(num_cpus=6)


@ray.remote
def single_trial(config_path, trial_index):
    with open(config_path, "r") as file:
        config = yaml.safe_load(file)

    config['trial_index'] = trial_index
    config['trial_config'] = config_path

    run_genetic_algorithm(config)
    return 1


config_paths = [
    './configs/bloat_experiments/soo_no_bloat_control.yaml',
    './configs/bloat_experiments/soo_double_tournament.yaml',
    './configs/bloat_experiments/soo_layer_weighted.yaml',
    './configs/bloat_experiments/soo_both.yaml',
    './configs/bloat_experiments/moo_no_bloat_control.yaml',
    './configs/bloat_experiments/moo_just_layer_weighted.yaml',
    './configs/bloat_experiments/moo_just_depth_objective.yaml',
    './configs/bloat_experiments/moo_layer_and_depth.yaml',
]

k = 20

print("Starting runs...")

futures = [single_trial.remote(config_path, trial_index) for config_path in config_paths for trial_index in range(k)]

print(ray.get(futures))

print("Done with all runs!")
