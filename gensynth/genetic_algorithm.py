import os
import uuid

import jax
import librosa
import numpy as np
import yaml
from deap import creator, base, gp, tools
from deap.tools import HallOfFame

from gensynth.differentiable.initialization import initialize_parameters
from gensynth.metrics.pickle_metrics import PickleLogging
from gensynth.metrics.tensorboard_metrics import TBLogging
from gensynth.metrics.tune_metric import TuneLogging
from gensynth.operators.genetic_operations import apply_genetic_operators
from gensynth.operators.selection import perform_selection


def run_genetic_algorithm(config):
    # Fix for DEAP storing ephemeral stuff globally and complaining about redefining them.
    gp_global_variables = gp.__dict__
    if "frequency" in gp_global_variables:
        del gp_global_variables["frequency"]
    if "multiplier" in gp_global_variables:
        del gp_global_variables["multiplier"]

    if isinstance(config, str):
        config_path = config
        config_name = os.path.basename(config_path).replace(".yaml", "")

        with open(config, "r") as file:
            config = yaml.safe_load(file)

        print(f"Loaded config  {config_name}")
    else:
        # If there is trial information given, we can get information from that.
        # Otherwise, the config path and name are just unknown, and certain
        # logging that requires it just can't be used.
        if config.get('trial_index') is not None:
            config_path = config['trial_config']
            config_name = f'{os.path.basename(config_path).replace(".yaml", "")}/{config["trial_index"]:02d}'
        print(f"Received config dict.")

    ea_config = config['evolutionary_algorithm']
    gp_config = ea_config['genetic_programming']
    memetic_config = config['jax']['lifetime_learning']

    # Use different functions depending on the implementation
    if config['jax']['use_jax_implementation']:
        # Tell TF to stay away from the GPU, as we only use TensorBoard anyways.
        try:
            import tensorflow as tf
            tf.config.set_visible_devices([], 'GPU')
        except Exception as e:
            print('Could not disable TensorFlow devices:', e)

        # Preallocate less of the GPU VRAM, as otherwise JAx tries to take too much and errors
        # os.environ['XLA_PYTHON_CLIENT_MEM_FRACTION'] = '0.4'

        # Disables preallocation at the risk of fragmentation and resulting OOMs
        os.environ['XLA_PYTHON_CLIENT_PREALLOCATE'] = 'false'

        from differentiable.representation import get_primitive_set
        from differentiable.fitness import reevaluate_individuals
        from differentiable.lifetime_learning import apply_lifetime_learning

        # Necessary because JAX converts float64 numbers into float32 numbers otherwise,
        # which can lead to differences in results.
        # However, this also drastically slows down loss evaluation.
        # from jax import config
        # config.update("jax_enable_x64", True)

        temporary_path = None
    else:
        from supercollider.representation import get_primitive_set
        from supercollider.synthesis import initialize_synthesis_dirs, remove_synthesis_dirs
        from supercollider.fitness import reevaluate_individuals

        # Use different temp paths to avoid collisions
        temporary_path = f"/tmp/synthesis/{uuid.uuid4()}/"

        initialize_synthesis_dirs(temporary_path)

    # Load the target audio properties and actual audio
    label_audio_name = config['target_audio']['name']
    sampling_rate = config['target_audio']['sample_rate']

    label_audio_path = "./sounds/" + label_audio_name + ".aiff"
    label_audio, _ = librosa.load(label_audio_path, sr=sampling_rate)
    duration = librosa.get_duration(y=label_audio, sr=sampling_rate)

    use_multiobjectivization = (len(ea_config['fitness_function']['feature']) > 1)

    # Set up logging
    if config['log_mode'] == 'tensorboard':
        logging = TBLogging(config_name, sampling_rate, config['jax']['use_jax_implementation'], use_multiobjectivization)
    elif config['log_mode'] == 'pickle':
        logging = PickleLogging(config_name, config_path)
    elif config['log_mode'] == 'tune':
        logging = TuneLogging()
    else:
        raise ValueError(f"Unknown logging mode '{config['log_mode']}'!")

    if not config['jax']['use_jax_implementation']:
        primitive_set = get_primitive_set()
    else:
        primitive_set = get_primitive_set(config['jax']['parameter_count'])

    # Create a fitness class for minimization (with a negative weight) for each objective
    creator.create("FitnessMin", base.Fitness, weights=[-1.0] * len(ea_config['fitness_function']['feature']))

    # Create an individual class containing a syntax tree, evaluated with the fitness class from above
    creator.create("Individual", gp.PrimitiveTree, fitness=creator.FitnessMin, pset=primitive_set)

    toolbox = base.Toolbox()
    # Create a function that generates a tree by growing it with the primitive set, using the configured min/max depth
    toolbox.register(
        "generateSynthTree",
        gp.genGrow,
        pset=primitive_set,
        min_=gp_config['tree_depth']['initial']['min'],
        max_=gp_config['tree_depth']['initial']['max']
    )

    # Create an individual generating function that is a synth definition tree
    toolbox.register("individual", tools.initIterate, container=creator.Individual, generator=toolbox.generateSynthTree)

    # Create a population function that generates a list of n individuals using the individual() function.
    toolbox.register("population", tools.initRepeat, container=list, func=toolbox.individual)

    print("Initializing population...")

    population = toolbox.population(n=ea_config['population_size'])

    if not use_multiobjectivization or ea_config['fitness_function']['nd_rank_double_tournament_selection'] is True:
        hall_of_fame = HallOfFame(maxsize=ea_config['selection']['hall_of_fame_size'])
    else:
        hall_of_fame = None

    if config['jax']['use_jax_implementation']:
        initialize_parameters(population, config)

    # Initial fitness evaluation
    reevaluate_individuals(
        population,
        label_audio,
        primitive_set,
        duration,
        sampling_rate,
        ea_config['fitness_function'],
        temporary_path=temporary_path
    )

    # Variables for early stopping
    best_fitness = None
    generations_since_improvement = 0

    for generation in range(ea_config['generation_count']):

        print("Generation " + str(generation))

        if not use_multiobjectivization or ea_config['fitness_function']['nd_rank_double_tournament_selection'] is True:
            hall_of_fame.update(population)

        print("- Generating offspring...")

        offspring = apply_genetic_operators(population, config, primitive_set, toolbox)

        # Evaluate all individuals whose fitness needs to be updated
        invalid_fitness_individuals = [individual for individual in offspring if not individual.fitness.valid]

        reevaluate_individuals(
            invalid_fitness_individuals,
            label_audio,
            primitive_set,
            duration,
            sampling_rate,
            ea_config['fitness_function'],
            temporary_path=temporary_path
        )

        # Perform lifetime learning and selection on the combined population
        population = list(map(toolbox.clone, population))
        offspring.extend(population)

        if config['jax']['use_jax_implementation'] and memetic_config['gradient_descent_steps'] > 0:
            offspring = apply_lifetime_learning(offspring, label_audio, duration, primitive_set, config, toolbox, logging)

        offspring = perform_selection(offspring, config, hall_of_fame)

        # Replace the population by the offspring
        population[:] = offspring

        print("- Logging")

        logging.log_generation(generation, population)

        # Apply early stopping
        if best_fitness is not None and generations_since_improvement > ea_config['early_stopping']:
            print(f"No improvement for {generations_since_improvement} generations, stopping.")
            break
        else:
            current_best_fitness = np.min([individual.fitness.values[0] for individual in population])
            if best_fitness is None or current_best_fitness < best_fitness:
                best_fitness = current_best_fitness
                generations_since_improvement = 0
            else:
                generations_since_improvement += 1

        print("- Generation Done.")

        # Clear JAX caches on suspicion of causing memory leakage with its compilation caches.
        if config['jax']['use_jax_implementation'] and generation % 10 == 0:
            jax.clear_caches()

    # Remove temporary directories. As JAX doesn't save any files, we don't need to remove anything there.
    if not config['jax']['use_jax_implementation']:
        remove_synthesis_dirs(temporary_path)
