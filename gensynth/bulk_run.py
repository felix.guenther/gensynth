import os

import ray

from gensynth.genetic_algorithm import run_genetic_algorithm

# Disable killing due to OOM
os.environ['RAY_memory_monitor_refresh_ms'] = '0'

ray.init(num_cpus=6)


@ray.remote
def single_run(config_path):
    run_genetic_algorithm(config_path)
    return 1


config_paths = [
    #'./configs/bloat_experiments/soo_no_bloat_control.yaml',
    #'./configs/bloat_experiments/soo_double_tournament.yaml',
    #'./configs/bloat_experiments/soo_layer_weighted.yaml',
    './configs/bloat_experiments/soo_both.yaml',
]

print("Starting runs...")

futures = [single_run.remote(config_path) for config_path in config_paths]

print(ray.get(futures))

print("Done with all runs!")
