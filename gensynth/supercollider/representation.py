import operator
import random

import supriya.ugens as ugens
from deap.gp import PrimitiveSetTyped, PrimitiveTree, genGrow

# A list of ugen tuples with the ugen, parameter types and the output type
available_ugens = [
    (ugens.SinOsc, [float, float], float),
    (ugens.Pulse, [float, float], float),
    (ugens.WhiteNoise, [], float),
    (ugens.BrownNoise, [], float),
    (ugens.BPF, [float, float, float], float),
    (ugens.BRF, [float, float, float], float)
]

def get_primitive_set():
    primitive_set = PrimitiveSetTyped("main", [], float)

    primitive_set.addPrimitive(operator.mul, [float, float], float)
    primitive_set.addPrimitive(operator.add, [float, float], float)
    primitive_set.addPrimitive(operator.sub, [float, float], float)

    for (ugen, parameter_types, return_type) in available_ugens:
        add_ugen(primitive_set, ugen, parameter_types, return_type)


    # Constants that may be used as multipliers
    primitive_set.addEphemeralConstant("multiplier", lambda: random.uniform(0.0, 5.0), ret_type=float)
    # Constants that may be used as an audible frequency
    primitive_set.addEphemeralConstant("frequency", lambda: float(random.randint(20, 20_000)), ret_type=float)

    return primitive_set


def add_ugen(primitive_set, ugen, parameter_types, return_type):
    # Add both audio rate and control rate variants and name them accordingly.
    # Note: As these are function names but not direct maps to Python code,
    # we can't just append the ar/kr with a dot. Otherwise, the eval() function later throws a fit.
    primitive_set.addPrimitive(
        primitive=ugen.ar,
        name=ugen.__name__ + "AR",
        in_types=parameter_types,
        ret_type=return_type
    )

    primitive_set.addPrimitive(
        primitive=ugen.kr,
        name=ugen.__name__ + "KR",
        in_types=parameter_types,
        ret_type=return_type
    )


def test():
    primitive_set = get_primitive_set()
    # Generate a tree randomly
    generated_tree = PrimitiveTree(genGrow(primitive_set, min_=1, max_=5))

    code = str(generated_tree)
    print(code)

    synth_def = eval(code, primitive_set.context, {})

    print(str(synth_def))
