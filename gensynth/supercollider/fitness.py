
import numpy as np
import scipy
from librosa import zero_crossings, power_to_db
from librosa.feature import melspectrogram, mfcc

from gensynth.supercollider.synthesis import individuals_to_sounds


# Output spectrograms must have the format (individuals, ...) even if there is only one individual.
def compute_spectrogram_distances(output_batch_spectrograms, label_spectrogram, norm):
    # The (single) label is broadcast over the spectrograms for each output
    difference = label_spectrogram - output_batch_spectrograms

    if norm in ['l1', 'l1_l2']:
        distance_l1 = np.linalg.norm(
            difference,
            # The first axis is omitted from the norm as we want to compute the distance
            # separately for each output.
            # The last axis is omitted to compute the norm for each time step separately
            axis=(1,),
            ord=1
        )

    if norm in ['l2', 'l1_l2']:
        distance_l2 = np.linalg.norm(
            difference.astype(float),
            # The first axis is omitted from the norm as we want to compute the distance
            # separately for each output.
            # The last axis is omitted to compute the norm for each time step separately
            axis=(1,),
            ord=2
        )

    # Sum over all time steps for each individual
    if norm == 'l1_l2':
        # Use both an L1 norm and an L2 norm to penalize few larger differences more strongly than smaller ones (L2)
        # but also penalize values in [0, 1] (L1).
        total = np.sum(
            distance_l1 + distance_l2,
            axis=(1,)
        )
    elif norm == 'l1':
        total = np.sum(
            distance_l1,
            axis=(1,)
        )
    elif norm == 'l2':
        total = np.sum(
            distance_l2,
            axis=(1,)
        )
    else:
        raise ValueError(f"Unknown distance {norm}")

    return total


def compute_zero_crossing_histogram(audio, bin_count):
    zero_crossings_mask = zero_crossings(audio, axis=-1)
    intervals = (np.diff(np.where(zero_crossings_mask == 1)) - 1)[0]
    # Reasoning:
    # zero_crossings_per_period = 2
    # lowest_audible_frequency = 20  # Hz
    # lowest_zero_crossings_per_second = zero_crossings_per_period * lowest_audible_frequency
    # longest_possible_interval = sample_rate / lowest_zero_crossings_per_second
    # longest_possible_interval = 1102.5
    zero_crossing_range = (0, 1103)
    histogram, _ = np.histogram(intervals, bins=bin_count, range=zero_crossing_range)
    return histogram


def compute_zero_crossing_losses(label_audio, output_audios, bin_count):
    # Output: Array of booleans indicating whether zero was crossed at a particular sample or not
    label_zc_histogram = compute_zero_crossing_histogram(label_audio, bin_count)

    distances = []
    for output_audio in output_audios:
        # TODO Try out different distance metrics
        output_zc_histogram = compute_zero_crossing_histogram(output_audio, bin_count)
        distance = scipy.stats.wasserstein_distance(label_zc_histogram, output_zc_histogram)
        distances.append(distance)

    return np.array(distances)


def compute_sound_losses(label_audio, individuals, output_audio, fitness_config, sampling_rate):
    # FIXME This shouldn't be recomputed, as the label doesn't change over training
    label_mel_spectrogram = melspectrogram(y=label_audio, sr=sampling_rate)
    output_mel_spectrograms = melspectrogram(y=output_audio, sr=sampling_rate)

    individual_count = output_audio.shape[0]
    fitness_count = len(fitness_config['feature'])
    losses = np.empty(shape=(individual_count, fitness_count))

    loss_index = 0

    # Compute the array of losses
    if 'mel' in fitness_config['feature']:
        losses[:, loss_index] = compute_spectrogram_distances(
            output_mel_spectrograms,
            label_mel_spectrogram,
            fitness_config['distance']
        )
        loss_index += 1

    if 'mel_db' in fitness_config['feature']:
        db_output_mel_spectrograms = power_to_db(output_mel_spectrograms, ref=np.max)
        db_label_mel_spectrogram = power_to_db(label_mel_spectrogram, ref=np.max)
        losses[:, loss_index] = compute_spectrogram_distances(
            db_output_mel_spectrograms,
            db_label_mel_spectrogram,
            fitness_config['distance']
        )
        loss_index += 1

    if 'mfcc' in fitness_config['feature']:
        label_mfcc = mfcc(y=label_audio, sr=sampling_rate)
        output_mfcc = mfcc(y=output_audio, sr=sampling_rate)
        losses[:, loss_index] = compute_spectrogram_distances(
            output_mfcc,
            label_mfcc,
            fitness_config['distance']
        )
        loss_index += 1

    if 'zero_crossings' in fitness_config['feature']:
        losses[:, loss_index] = compute_zero_crossing_losses(
            label_audio,
            output_audio,
            bin_count=fitness_config['zero_crossings_histogram_bin_count']
        )
        loss_index += 1

    if 'depth' in fitness_config['feature']:
        losses[:, loss_index] = np.array([individual.height for individual in individuals])
        loss_index += 1

    # Set the loss for invalid definitions to infinity.
    # This is not very efficient yet as  we still compute the fitnesses,
    # just on an empty signal. However, since invalid individuals should be sorted out
    # quickly anyways, that shouldn't make too much of an impact.
    losses[np.max(output_audio, axis=1) == 0, :] = np.inf

    return losses


def reevaluate_individuals(
    individuals,
    label_audio,
    primitive_set,
    duration,
    sampling_rate=44100,
    fitness_config=None,
    temporary_path="/tmp/synthesis/",
    sc_executable_path="./SuperCollider/installation/bin/scsynth"
):

    if len(individuals) == 0:
        return []

    print("-- Generating Audio")

    output_audio = individuals_to_sounds(individuals, primitive_set, duration, sampling_rate, temporary_path, sc_executable_path)

    print("-- Computing Losses")

    losses = compute_sound_losses(label_audio, individuals, output_audio, fitness_config, sampling_rate)

    for index, (individual, losses, audio) in enumerate(zip(individuals, losses, output_audio)):
        individual.fitness.values = losses

        # Save the audio for logging purposes
        individual.audio = audio
