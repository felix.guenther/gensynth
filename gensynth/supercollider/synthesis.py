import asyncio
import glob
import numbers
import os
import shutil
from pathlib import Path
from typing import List

import librosa
import numpy as np
import supriya
from deap.gp import PrimitiveTree
from supriya import SynthDefBuilder, CalculationRate
from supriya.ugens import Out, K2A


def initialize_synthesis_dirs(temporary_path="/tmp/synthesis/"):
    render_path = temporary_path + "render/"
    output_path = temporary_path + "results/"
    # Create necessary directories
    Path(render_path).mkdir(parents=True, exist_ok=True)
    Path(output_path).mkdir(parents=True, exist_ok=True)


def remove_synthesis_dirs(temporary_path="/tmp/synthesis/"):
    shutil.rmtree(temporary_path)


def synth_tree_to_synth_def(synth_tree: PrimitiveTree, index, primitive_set):
    builder = SynthDefBuilder()
    with builder:
        synth_out = eval(
            str(synth_tree),
            primitive_set.context,
            {}
        )

        # Stopgap to avoid piping something that is just a constant into Out.ar, which causes a SC segfault
        if isinstance(synth_out, numbers.Number):
            return None

        # Convert a control rate root node to audio rate to avoid a segfault
        if synth_out.calculation_rate == CalculationRate.CONTROL:
            synth_out = K2A.ar(source=synth_out)

        # Direct each synth to its own output bus
        Out.ar(bus=index, source=synth_out)

    return builder.build(name="synth_" + str(index))


def individuals_to_sounds(
    individuals,
    primitive_set,
    duration,
    sampling_rate,
    temporary_path="/tmp/synthesis/",
    sc_executable_path="./SuperCollider/installation/bin/scsynth"
):
    # Convert the individual trees to synth definitions (or None, for invalid individuals)
    synth_definitions = [
        synth_tree_to_synth_def(individual, index, primitive_set)
        for index, individual in enumerate(individuals)
    ]

    # Save synth definition for logging purposes
    for individual, synth_definition in zip(individuals, synth_definitions):
        individual.synth_definition = synth_definition

    output_audio, exit_code = synthesize_sounds(
        # Filter out all invalid synth defs
        list(filter(lambda synth_def: synth_def is not None, synth_definitions)),
        duration,
        channel_count=len(individuals),
        sampling_rate=sampling_rate,
        temporary_path=temporary_path,
        sc_executable_path=sc_executable_path
    )

    if exit_code != 0:
        print("Error: Synthesis produced exit code " + str(exit_code))
        print("Synths:")
        [print(str(synth)) for synth in synth_definitions]
        exit(1)

    # Add the individual dimension even if the audio itself only contains one
    if len(output_audio.shape) == 1:
        output_audio = np.expand_dims(output_audio, 0)

    return output_audio


def synthesize_sounds(
    synth_definitions: List,
    duration: float,
    channel_count,
    sampling_rate=44100,
    temporary_path="/tmp/synthesis/",
    sc_executable_path="./SuperCollider/installation/bin/scsynth"
):
    render_path = temporary_path + "render/"
    output_path = temporary_path + "results/"
    absolute_executable_path = os.path.abspath(sc_executable_path)

    # SuperNova is much slower to boot than scsynth! Rendering comparisons for complex synths remain to be made.
    score = supriya.Score(executable=absolute_executable_path)

    # Perform the operations at moment (timestamp) 0
    with score.at(0):
        # Add the synthdefs and wait for that process to complete
        with score.add_synthdefs(*synth_definitions):

            # Use the synths
            group = score.add_group(parallel=True)
            for synth in synth_definitions:
                _ = group.add_synth(synth)

    coroutine = score.render(
        duration=duration,
        sample_rate=sampling_rate,
        output_file_path=output_path + "synthesis_result.aiff",
        # Ensure that the rendering path is also in /tmp, as the output is only copied to the final path, not written
        # there directly.
        render_directory_path=render_path,
        output_bus_channel_count=channel_count,
        audio_bus_channel_count=channel_count + 16
    )

    output_file, exit_code = asyncio.run(coroutine)

    if exit_code == 0:
        # Audio: A time series in form of a 1D NumPy float array
        # Each channel contains a separate (mono) sound.
        output_audio, _ = librosa.load(output_file, sr=sampling_rate, mono=False)

        # Drop the last samples if SC generates more samples than we requested
        sample_count = int(sampling_rate * duration)
        if output_audio.shape[-1] > sample_count:
            output_audio = output_audio[..., :sample_count]

    else:
        output_audio = None

    # Remove intermediate files
    files = glob.glob(render_path + '*')
    for file in files:
        os.remove(file)

    # Remove rendered files as well, they'll be written again from memory if needed.
    # Leaving them risks them staying in memory if the learning gets cancelled prematurely.
    files = glob.glob(output_path + '*')
    for file in files:
        os.remove(file)

    return output_audio, exit_code
