import datetime
import os
import argparse

import yaml
from ray import tune, air
from ray.tune.schedulers import AsyncHyperBandScheduler
from ray.tune.search.optuna import OptunaSearch

from gensynth.genetic_algorithm import run_genetic_algorithm

# Disable killing due to OOM
os.environ['RAY_memory_monitor_refresh_ms'] = '0'

parser = argparse.ArgumentParser()
parser.add_argument("--config", type=str, default='./configs/tune_base.yaml')
args = parser.parse_args()
config_path = args.config

with open(config_path, "r") as file:
    parameter_space = yaml.safe_load(file)


# Effectively disable early stopping, as ASHA does that for us.
parameter_space['evolutionary_algorithm']['early_stopping'] = 1000


# Select a subset of options according to tune.
def sample_subset(choice_set):
    possible_choices = [[]]
    for option in choice_set:
        for possible_choice in possible_choices.copy():
            new_choice = possible_choice.copy()
            new_choice.append(option)
            possible_choices.append(new_choice)

    return tune.choice(possible_choices)


parameter_space['evolutionary_algorithm']['operators'] = {
    'crossover': sample_subset(['one_point', 'merge', 'merge_arithmetic']),
    'mutation': sample_subset(['uniform', 'shrink', 'replace', 'insert', 'layer_weighted', 'ephemeral']),
    'crossover_probability': tune.quniform(0.05, 0.5, 0.05),
    'mutation_probability': tune.quniform(0.05, 0.5, 0.05)
}

parameter_space['evolutionary_algorithm']['selection'] = {
    'scheme': 'rank_double_tournament',
    'fitness_tournament_size': tune.lograndint(3, 15),
    'size_tournament_size': tune.quniform(1.1, 1.8, 0.1),
    'hall_of_fame_size': 5
}

parameter_space['supercollider']['eph_mode'] = tune.choice(['all', 'one'])

parameter_space['multi_objective'] = {
    'helper_objective': tune.choice(['mfcc', 'zero_crossings', None]),
    'zero_crossings_histogram_bin_count': tune.qrandint(10, 200, 10),
    'nd_rank_double_tournament_selection': tune.choice([False, True])
}

# Run for at least grace-period generations and at most max_t.
# Smartly apply early stopping.
scheduler = AsyncHyperBandScheduler(grace_period=30, max_t=300)

algo = OptunaSearch()

# Repeat experiments 5 times to ensure that we measure actual improvements and not luck
repeated_alg = tune.search.Repeater(algo, repeat=5)

tune_config = tune.TuneConfig(
    search_alg=repeated_alg,
    metric="best_fitness",
    mode="min",
    # Sample each configuration this often (-1 for infinite until time runs out)
    num_samples=-1,
    time_budget_s=1.8 * 60 * 60,
    max_concurrent_trials=6,
    chdir_to_trial_dir=False,
    scheduler=scheduler
)

run_config = air.RunConfig(
    name=f"genetic_algorithm_trial_{datetime.datetime.now():%Y_%m_%d__%H:%M}",
    storage_path="./logs/ray_results",
    log_to_file=True
)

tuner = tune.Tuner(
    run_genetic_algorithm,
    run_config=run_config,
    tune_config=tune_config,
    param_space=parameter_space
)

results = tuner.fit()
print("Done! Best results: ", results.get_best_result().config)
