import unittest

from hypothesis import given
from hypothesis.extra.numpy import arrays, from_dtype
from hypothesis.strategies import shared, integers, tuples
import numpy as np

from gensynth.supercollider.fitness import compute_spectrogram_distances


class TestFitness(unittest.TestCase):

    channel_count = shared(integers(min_value=1, max_value=16))
    bucket_count = shared(integers(min_value=1, max_value=64))
    time_steps = shared(integers(min_value=1, max_value=32))

    spectrogram_element_strategy = from_dtype(np.dtype(np.float32), allow_nan=False, allow_infinity=False)
    spectrogram = arrays(
        dtype=np.dtype,
        elements=spectrogram_element_strategy,
        shape=tuples(channel_count, bucket_count, time_steps)
    )

    @given(output_spectrogram=spectrogram, label_spectrogram=spectrogram)
    def test_spectrogram_distance_shape(self, output_spectrogram, label_spectrogram):
        result = compute_spectrogram_distances(output_spectrogram, label_spectrogram)
        assert isinstance(result, np.ndarray), "Distance has unexpected type " + str(result.__class__)
        assert result.shape == (output_spectrogram.shape[0],), "Unexpected shape " + str(result.shape)

    @given(output_spectrogram=spectrogram, label_spectrogram=spectrogram)
    def test_spectrogram_distance_is_not_negative(self, output_spectrogram, label_spectrogram):
        result = compute_spectrogram_distances(output_spectrogram, label_spectrogram)
        assert (result >= 0).all(), str(result) + " is not >= 0"

    def test_basic_gradient(self):
        spectrogram = np.array([
            [
                [1, 2, 1],
                [1, 2, 1]
            ],
            [
                [2, 2, 2],
                [2, 2, 2]
            ]
        ])

        label = np.array([
            [1, 1, 1],
            [1, 1, 1]
        ])

        fitnesses = compute_spectrogram_distances(spectrogram, label)
        assert \
            fitnesses[0] < fitnesses[1], \
            "Fitness has unexpected gradient! f0: " + str(fitnesses[0]) + " f1: " + str(fitnesses[1])

    # Ensure that multiple smaller differences are less penalized than one large difference of the same magnitude
    def test_severity_gradient(self):
        spectrogram = np.array([
            [
                [1, 2, 1],
                [1, 2, 1]
            ],
            [
                [3, 1, 1],
                [1, 1, 1]
            ]
        ])

        label = np.array([
            [1, 1, 1],
            [1, 1, 1]
        ])

        fitnesses = compute_spectrogram_distances(spectrogram, label)
        assert \
            fitnesses[0] < fitnesses[1], \
            "Fitness has unexpected gradient! f0: " + str(fitnesses[0]) + " f1: " + str(fitnesses[1])

    # Ensure that the severity gradient also works for differences in [0, 1]
    def test_small_severity_gradient(self):
        spectrogram = np.array([
            # Channel 0
            [
                # Bucket 0
                [1, 1.2, 1],
                # Bucket 1
                [1, 1.2, 1]
            ],
            # Channel 1
            [
                # Bucket 0
                [1.4, 1, 1],
                # Bucket 1
                [1, 1, 1]
            ]
        ])

        label = np.array([
            [1, 1, 1],
            [1, 1, 1]
        ])

        fitnesses = compute_spectrogram_distances(spectrogram, label)
        assert \
            fitnesses[0] < fitnesses[1], \
            "Fitness has unexpected gradient! f0: " + str(fitnesses[0]) + " f1: " + str(fitnesses[1])