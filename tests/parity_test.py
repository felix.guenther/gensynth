import unittest

import hypothesis as hp
import librosa
import numpy as np
from hypothesis import given, settings
from librosa.feature import melspectrogram
from supriya import synthdef
from supriya.ugens import SinOsc, Out

from gensynth.differentiable.fitness import mel_spectrogram
from gensynth.differentiable import sine_wave
from gensynth.supercollider import initialize_synthesis_dirs, synthesize_sounds


# Tests that ensure that the JAX implementation generates equivalent results to SuperCollider.
class ParityTest(unittest.TestCase):

    @given(
        frequency=hp.strategies.floats(min_value=20.0, max_value=20000.0, allow_nan=False, allow_infinity=False),
        phase=hp.strategies.floats(allow_nan=False, allow_infinity=False, max_value=10000.0, min_value=-10000.0)
    )
    @settings(deadline=None)
    def test_static_sine_parity(self, frequency, phase):
        duration = 0.1
        sample_rate = 44100

        # TODO Fix tests failing due to undefined global variables

        jax_audio = sine_wave(
            frequency=frequency,
            phase=phase
        )

        phase = phase % (2 * np.pi)

        @synthdef()
        def basic_oscillation():
            sin = SinOsc.ar(frequency=frequency, phase=phase)
            Out.ar(bus=0, source=sin)

        initialize_synthesis_dirs()

        synthesize_sounds([basic_oscillation], duration, 1, sampling_rate=sample_rate)
        sample_path = "/tmp/synthesis/results/synthesis_result.aiff"

        # Note: Without specifying the duration, we get 6 extra samples!
        sc_audio, _ = librosa.load(sample_path, duration=duration, sr=sample_rate)

        assert jax_audio.shape[0] == sc_audio.shape[0]
        # Only test equality up to a certain precision
        np.testing.assert_allclose(jax_audio, sc_audio, atol=5e-02)

    @given(
        frequency=hp.strategies.floats(min_value=20.0, max_value=20000.0, allow_nan=False, allow_infinity=False),
        phase=hp.strategies.floats(allow_nan=False, allow_infinity=False, max_value=10000.0, min_value=-10000.0)
    )
    @settings(deadline=None)
    def test_temporal_sine_parity(self, frequency, phase):
        duration = 0.1
        sample_rate = 44100

        sine1 = sine_wave(
            frequency=frequency,
            phase=phase
        )
        jax_audio = sine_wave(
            frequency=sine1 * 440.0,
            phase=0.0
        )

        phase = phase % (2 * np.pi)

        @synthdef()
        def basic_oscillation():
            sin1 = SinOsc.ar(frequency=frequency, phase=phase)
            sin2 = SinOsc.ar(frequency=sin1 * 440.0, phase=0.0)
            Out.ar(bus=0, source=sin2)

        initialize_synthesis_dirs()

        synthesize_sounds([basic_oscillation], duration, 1, sampling_rate=sample_rate)
        sample_path = "/tmp/synthesis/results/synthesis_result.aiff"

        # Note: Without specifying the duration, we get 6 extra samples!
        sc_audio, _ = librosa.load(sample_path, duration=duration, sr=sample_rate)

        # For debugging
        # plt.plot(time, jax_audio)
        # plt.plot(time, sc_audio)
        # plt.show()

        assert jax_audio.shape[0] == sc_audio.shape[0]
        # Only test equality up to a certain precision
        np.testing.assert_allclose(jax_audio, sc_audio, atol=1e-01)

    def test_mel_spectrogram_parity(self, frequency=20.0, phase=1.0):
        duration = 0.1
        sample_rate = 44100

        # Generate a sample sound
        @synthdef()
        def basic_oscillation():
            sin1 = SinOsc.ar(frequency=frequency, phase=phase)
            sin2 = SinOsc.ar(frequency=sin1 * 440.0, phase=0.0)
            Out.ar(bus=0, source=sin2)

        initialize_synthesis_dirs()

        synthesize_sounds([basic_oscillation], duration, 1, sampling_rate=sample_rate)
        sample_path = "/tmp/synthesis/results/synthesis_result.aiff"

        # Note: Without specifying the duration, we get 6 extra samples!
        audio, _ = librosa.load(sample_path, duration=duration, sr=sample_rate)

        librosa_mel_spectrogram = melspectrogram(y=audio, sr=sample_rate)
        jax_mel_spectrogram = mel_spectrogram(audio, sample_rate)

        self.assertEqual(librosa_mel_spectrogram.shape, jax_mel_spectrogram.shape)

        np.testing.assert_allclose(jax_mel_spectrogram, librosa_mel_spectrogram, atol=5e-04)
