# Learns noise of two slightly different types across the PF during one run.
# During another, it finds interesting (but wrong) solutions.
# Solution complexity increases a lot after no further improvements are found
# Diversity stays decent until the end.

target_audio:
  # An audio file stored in the sounds directory
  name: synthetic/multiple_oscillations
  sample_rate: 44100


evolutionary_algorithm:
  population_size: 500
  generation_count: 300
  # After this many generations without an improvement in the solution, the process is stopped prematurely.
  early_stopping: 10

  operators:
    # one_point
    crossover: one_point
    # uniform, layer_weighted
    mutation: uniform
    # Probabilities of the operators. These are also used for the extra mutation/crossover
    # operations performed in the implementation-specific variants.
    crossover_probability: 0.5
    mutation_probability: 0.5

  # One of mel, mfcc, mel_db
  fitness_function:
    distance: l1_l2
    feature: mel_db

  # Only relevant if no helper-objective is used, as otherwise NSGA2 selection is done
  selection:
    # tournament, double_tournament, rank_double_tournament
    scheme: tournament
    fitness_tournament_size: 3
    # A number in [1, 2]. The smaller individual is selected with a probability of size_tournament_size/2
    # Only used in the double tournament schemes.
    size_tournament_size: 1.2
    # Corresponds to (not) using k-elitism
    hall_of_fame_size: 5

  genetic_programming:
    tree_depth:
      initial:
        min: 1
        max: 3
      # Limit on the tree depth that can be reached during learning.
      # The hard limit here is 90, as that is the maximum call stack depth Python permits.
      max: 17

supercollider:
  # The SC implementation uses ephemeral constants, which are mutated in an extra step.
  # There are the modes all or one for this mutation.
  eph_mode: "all"

jax:
  use_jax_implementation: no
  # The JAX implementation uses a set of parameters instead of random
  # constants in the tree. These get their own simulated binary crossover
  # and mutation (reinitializing random values) as an extra step.
  parameter_count: 5
  simulated_binary_crossover_eta: 2
  lifetime_learning:
    individual_count: 10
    gradient_descent_steps: 200
    # Starting learning rate for the Adam optimizer
    initial_learning_rate: 5e-4

multi_objective:
  # Possible values: mfcc, zero_crossings, or empty.
  helper_objective: mfcc


log_mode: pickle