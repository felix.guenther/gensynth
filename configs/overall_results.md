# 1
Finding a solution for a very simple case is possible.
By default, learning converges on overly bloated solutions.

# 2
Double tournament selection combined with layer weighted mutation is an adequate bloat control mechanism.
Performing rank tournament selection instead of regular tournament selection preserves more diversity.

# 3
Both the mfcc and the mel_db config struggle severely with slightly more complex scenarios.
The bloat control mechanisms appear to restrict the search process some, but releasing that restriction
results in non-useful discoveries for the mel variant, and no meaningful difference for the mfcc one.

# 4
The zero crossing objective does not appear to add much value. Removing bloating control doesn't change that. It learns
little, and the preferences for solutions it does have do not yield useful results.
Combining mel and mfcc can find some interesting solutions, in particular when removing bloating control.
None are very good or much better than the solutions found by the simpler approach though.
Diversity is middling, depending on the run.

# 5
The combination/merging operator significantly improves the results!
Best results, at the cost of heavy bloating, were obtained so far in the MOO case with the merging operator.
Performing a single objective run with bloating didn't perform as good, as did a MOO run with an adjusted NSGA2 operator.
L1 distances for the spectrograms seem to work better than L2 distances.

# 6
NOTE: This dropped the ephemeral mutation operator!

Dropping the regular crossover operator found very different qualitative results across runs.
Combining the merge operator with shrink mutation does not restrict bloating.
Using the merge operator without bloat control and without MOO achieves a mediocre solution, suggesting that
the multi-objective approach is important to getting good results.

# 7
Combining the merge operator with shrink mutation does still not restrict bloating.
Again: Using the merge operator without bloat control and without MOO achieves a mediocre solution, suggesting that
the multi-objective approach is important to getting good results.

Using only arithmetic achieved a great result in one run, and a terrible one in another.
Omitting one-point-crossover did not learn good solutions.

# 8
Using depth as an additional objective seems to be effective bloat control.
Using Zero Crossings as an objective finds interestingly different solutions, but not necessarily better ones. Mileage varies.
Finding the good solutions we sometimes got seems to be very luck based.

The quadruple objective case seems promisingly robust against luck the first three tries!
