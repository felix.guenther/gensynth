# gensynth

This is the accompanying code for the thesis "Learning Sound Synthesizer structures using Genetic Programming".
The configuration files used in the experiments can be found in the `configs` directory. The Python code is in `gensynth`.
There is a division between the SuperCollider and JAX implementation of some functions, corresponding to the `supercollider`
and `differentiable` packages.

Various Jupyter notebooks used for the analysis of the results are located in `notebooks` (In particular `Evolution Lens`
being an important one), while the sounds used as targets can be found in `sounds`. 

## Setup
Install micromamba (or conda, if preferred), create an environment with Python 3.10, and run
`micromamba install deap librosa matplotlib numpy tensorboard tensorflow graphviz pygraphviz hypothesis jax optax tqdm pandas ipympl pyyaml jupyterlab ray-default ray-tune ray-air -c conda-forge`

tornado=6.1 to fix https://discourse.jupyter.org/t/jupyter-notebook-zmq-message-arrived-on-closed-channel-error/17869
(apparently not necessary with jupyterlab)

Install supriya via
`pip install supriya`

Setup SuperCollider (local compilation). See Supercollider_standalone.md.

JAX GPU:
micromamba install jaxlib=*=*cuda* jax cuda-nvcc -c conda-forge -c nvidia
pip install nvidia-cudnn-cu11==8.8.0.*
