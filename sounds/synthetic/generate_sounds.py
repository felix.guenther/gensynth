import shutil
import pygraphviz as pgv
import librosa
import soundfile as sf
import supriya
import matplotlib.pyplot as plt
from librosa.feature import melspectrogram
from supriya import synthdef
from supriya.ugens import SinOsc, Out, BrownNoise, BRF

from gensynth.metrics.tensorboard_metrics import plot_mel_spectrogram
from gensynth.supercollider.synthesis import initialize_synthesis_dirs, synthesize_sounds

# Run this script with the project root as working directory

sound_duration_seconds = 0.1


@synthdef()
def basic_oscillation():
    sine = SinOsc.ar(frequency=440)
    Out.ar(bus=0, source=sine* 0.1)


@synthdef()
def modulated_oscillation():
    modulation_sine = 2000 * SinOsc.ar(frequency=0.8)
    modulated_sine = SinOsc.ar(frequency=500.0 + modulation_sine)
    Out.ar(bus=0, source=modulated_sine * 0.1)


@synthdef()
def multiple_oscillations():
    sines = [SinOsc.ar(frequency=110 * i) for i in range(5)]
    added_sines = sum(sines)
    Out.ar(bus=0, source=added_sines * 0.1)


@synthdef()
def filtered_noise():
    noise = BrownNoise.ar()
    filtered = BRF.ar(source=noise, frequency=440.0)
    Out.ar(bus=0, source=filtered * 0.1)


@synthdef()
def fm_synthesis():
    modulation_sine = 2000 * SinOsc.ar(frequency=440.0)
    modulated_sine = SinOsc.ar(frequency=500.0 + modulation_sine)
    Out.ar(bus=0, source=modulated_sine * 0.1)


initialize_synthesis_dirs()

synth_defs = [
    basic_oscillation, multiple_oscillations, modulated_oscillation, filtered_noise, fm_synthesis,
    supriya.default
]

for synthesizer_definition in synth_defs:
    output_audio, exit_code = synthesize_sounds([synthesizer_definition], sound_duration_seconds, 1)
    sample_path = "./sounds/synthetic/" + synthesizer_definition.name + ".aiff"
    sf.write(sample_path, output_audio, samplerate=44100, format='aiff')

    label_mel_spectrogram = melspectrogram(y=output_audio, sr=44100)
    figure = plot_mel_spectrogram(mel_spectrogram=label_mel_spectrogram, sampling_rate=44100)
    plt.savefig(sample_path.replace(".aiff", "_mel_spectrogram.png"))
    plt.close(figure)

    uqbar_graph = synthesizer_definition.__graph__()
    graph_string = format(uqbar_graph, "graphviz")
    graph = pgv.AGraph(graph_string)
    png_image = graph.draw(
        path="./sounds/synthetic/" + synthesizer_definition.name + '_graph.png',
        format='png',
        prog="dot"
    )

    print(librosa.get_duration(y=output_audio, sr=44100))
