import os

import librosa
import matplotlib.pyplot as plt
import numpy as np

sampling_rate = 44100

directories = ['instruments', 'organic', 'percussive', 'tones']

for directory in directories:

    for audio_file in os.listdir(directory):

        audio_path = "./" + directory + '/' + str(audio_file)
        audio, _ = librosa.load(audio_path, sr=sampling_rate)

        duration = librosa.get_duration(y=audio, sr=sampling_rate)

        mel_spectrogram_label = librosa.feature.melspectrogram(y=audio, sr=sampling_rate)
        mel_spectrogram_decibel_label = librosa.power_to_db(mel_spectrogram_label, ref=np.max)
        figure, ax = plt.subplots()
        img = librosa.display.specshow(
            mel_spectrogram_decibel_label,
            x_axis='time',
            y_axis='mel',
            sr=sampling_rate,
            fmax=8000,
            ax=ax
        )
        figure.colorbar(img, ax=ax, format='%+2.0f dB')
        plt.tight_layout()
        ax.set(title='Mel-frequency spectrogram')

        spectrogram_file_name = audio_path.replace(".aiff", "_spectrogram")
        plt.savefig(spectrogram_file_name)
